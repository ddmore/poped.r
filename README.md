PopED (including beta PharmML support)
======

PopED computes optimal experimental designs for both population
and individual studies based on nonlinear mixed-effect models.
Often this is based on a computation of the Fisher Information Matrix (FIM).

This version includes pharmml2poped.exe (version 0.1.3) to enable PharmML
support in PopED.

## Build

To create an R source package, simply execute:
```
R CMD build source/
```

Note that `pharmml2poped.exe` is included in the source as a 64bit Windows executable
binary resource. It can't be rebuilt by this package from source (yet). Therefore this
package is *only compatible* with 64bit Windows.

## Install

To install (and possibly replace earlier installation) do one of the following:

1. Install from **source** package  
   ```
   install.packages("PopED_0.3.0.9509.tar.gz", repos=NULL, type="source")
   ```
2. Install from **binary** package  
     ```
     install.packages("PopED_0.3.0.9509.zip", repos=NULL, type="win.binary")
     ```  

     This package was built under Windows 10 and R 3.0.3 (64bit) using Rtools:  
     ```
     R CMD INSTALL --build PopED_0.3.0.9509.tar.gz
     ```

## Usage

To load a PharmML and automatically create the PopED objects, execute:
```
as.poped("pharmml_file.xml")
```

To generate an evaluatable R source file which creates the PopED objects execute:
```
as.poped("pharmml_file.xml", "")
```  
(Where the second argument may be a file name if another basename is desired.)

In case of an error in conversion the 1-argument version of `as.poped` will possibly fail loading while the 2-argument version will generate a possibly broken best-try conversion.

Please refer to [PopED on CRAN](https://cran.r-project.org/web/packages/PopED/index.html) for information on usage of PopED itself.
